fun main() {
    val range = 1..100
    val targetNumber = range.random()
    var attempts = 5

    println(
        "Bienvenido al Juego del Hambre. Tu vida está en riesgo." +
                "Tenés $attempts intentos para elegir un número." +
                "Spoilers: es ${targetNumber}"
            )

    do {
        var chosen: Int? = null
        while (chosen == null) {
            print("Te queda $attempts intento${if (attempts > 1) 's' else ""}. Por favor, ingresá un número entre ${range.first} y ${range.last}: ")
            chosen = readLine()!!.toIntOrNull()
        }

        val statusMessage = when {
            chosen > targetNumber -> "Te pasaste!"
            chosen < targetNumber -> "Te faltó!"
            else -> break
        }

        println(statusMessage)

    } while (--attempts > 0)

    val finalMessage = when {
        attempts > 0 -> "Ganaste, con $attempts intentos"
        else -> "Perdiste. Coma pan."
    }

    print("Terminó el juego! El número era $targetNumber. $finalMessage")
}
